#pragma once
#include "Engine.h"
#include "Txt_File.h"
#include "Subject.h"
#include "Output.h"
#include <sstream>
#include <iostream>
using namespace std;
/**
*  The main class we work on, understands and runs the input lines
*/
class Simulation : public Subject
{
private:
    Engine* engine;
	vector<Tank*> all_tanks;
	vector<string> command_list;
	int number_of_tanks = 0;
	int time = 0;
	vector<Observer*> observers;
public:
	Simulation() {
		this->engine = Engine::getInstance();
		Output* out = Output::getInstance();
	}
	void start_simulation(string, string);
	void stop_simulation();
	void list_connected_tanks();
	void add_fuel_tank(double);
	void list_fuel_tanks();
	void remove_fuel_tank(int);
	void print_fuel_tank_count();
	void wait(int);
	void run_lines();
	void fill_tank(int, double);
	void addObserver(Observer*);
	void removeObserver(int);
	void notify();
};

