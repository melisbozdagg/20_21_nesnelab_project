#include "Engine.h"

Engine* Engine::uniqueInstance = 0;
/**
*  getting instane of singleton class
*  @return  uniqueInstance
*/
Engine* Engine::getInstance() { 
	if (uniqueInstance == NULL) {
		uniqueInstance = new Engine();
	}
	return uniqueInstance;
}
/**
*  starts engine
*/
void Engine::start_engine()
{
	if (this->tanks.size()!=0) {
		this->status = 1;
	}
	else {
		Output* output = Output::getInstance();
		output->write("\nEngine can not start without at least one connected tank!\n");
	}
}
/**
*  stopss engine
*/
void Engine::stop_engine()
{
	int min = 0;
	for (int i = 0; i < this->tanks.size(); i++)
	{
		if (this->tanks[min]->get_fuel_quantity() > this->tanks[i]->get_fuel_quantity()) {
			min = i;
		}
	}
	this->tanks[min]->increase_quanity(this->internal_tank->get_fuel_quantity());
	this->internal_tank->increase_quanity(-this->internal_tank->get_fuel_quantity());
}
/**
*  adds tank to connected tanks
*/
void Engine::connect_tank(Tank* adress_of_tank)
{
	this->tanks.push_back(adress_of_tank);
}
/**
*  removes tank from connected tanks
*/
void Engine::disconnect_tank(Tank* ads_of_tank)
{
	for (int i = 0; i < this->tanks.size(); i++)
		if (this->tanks[i] == ads_of_tank) {
			this->tanks.erase(this->tanks.begin() + i);
			return;
		}
}
/**
*  prints total consumed fuel quantity
*/
void Engine::print_total_consumed_fuel_quantity(int time) {
	Output* op = Output::getInstance();
	ostringstream out;
	out << "Total consumed fuel quantitiy: " << double(this->fuel_per_second * time) << endl << endl;
	op->write(out.str());
}/**
*  prints totalfuel quantity
*/
void Engine::print_total_fuel_quantity() {
	Output* op = Output::getInstance();
	ostringstream out;
	double total_fuel_quantity = 0;
	for (int i = 0; i < tanks.size(); i++) {
		if (tanks[i] != NULL)
			total_fuel_quantity += tanks[i]->get_fuel_quantity();
	}

	out << "Total fuel quantity: " << total_fuel_quantity << endl << endl;
	op->write(out.str());
}

void Engine::give_back_fuel()
{
}

double Engine::get_fuel_per_second()
{
	return fuel_per_second;
}

bool Engine::get_status()
{
	return status;
}

Tank* Engine::get_internal_tank()
{
	return internal_tank;
}
/**
*  @return tank with minimum fuel quanity
*/
Tank * Engine::get_min_fuel_tank()
{
	int min_index = 0;
	for (int i = 1; i < this->tanks.size(); i++)
	{
		if (this->tanks[i]->get_fuel_quantity() < this->tanks[i]->get_fuel_quantity())
		{
			min_index = i;
		}
	}
	return this->tanks[min_index];
}
/**
*  picks tank to absorb fuel
*  @return tank to absorb its fuel
*/
Tank * Engine::absorbs_fuel()
{
	bool flag = 0;
	for (int i = 0; i < this->tanks.size(); i++)
		if (this->tanks[i]->get_fuel_quantity() > 20)
			flag = 1;
	if (flag == 0) {
		Output* op = Output::getInstance();
		op->write("Not enough fuel to keep working!!\n");
		exit(0);
	}
	int tank_index;
	srand(time(0));
	do {
		tank_index = rand() % (this->tanks.size());
	} while (this->tanks[tank_index]->get_fuel_quantity() < 20);
	return this->tanks[tank_index];
}
/**
*  burns fuel
*  @param time burns fuel for this time
*/
void Engine::burn_fuel(int time)
{
	this->internal_tank->increase_quanity(-time * this->fuel_per_second);
	if (this->internal_tank->get_fuel_quantity() < 20) {
		Tank* tank = this->absorbs_fuel();
		this->internal_tank->increase_quanity(tank->get_fuel_quantity());
		tank->increase_quanity(-tank->get_fuel_quantity());
	}
}
/**
*  notifys engine class
*/
void Engine::update()
{
	Output* output = Output::getInstance();
	output->write("Engine: Simulation stopped\n");
}

Engine::Engine()
{
}

