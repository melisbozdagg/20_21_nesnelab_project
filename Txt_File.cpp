#include "Txt_File.h"

Txt_File::Txt_File() {

}
Txt_File::~Txt_File() {

}

bool Txt_File::open_file(string file_name) {
	file.open(file_name);
	if (file.is_open())
		return 1;
	cout << "File reading error!\n";
	return 0;
}
vector<string> Txt_File::input() {
	Input inp1;
	inp1.set_file(file);
	return inp1.read_input();
}
void Txt_File::close_file() {
	file.close();
}