#include "Valve.h"

bool Valve::getStatus()
{
	return this->status;
}

void Valve::openValve()
{
	this->status = 1;
}

void Valve::closeValve()
{
	this->status = 0;
}
/**
*  updates oserver, will be called from its tank
*  @param index for printing with index
*/
void Valve::update(int index)
{
	ostringstream out;
	Output* output = Output::getInstance();
	out << "Valve " << index + 1 << ":Simulation stopped" << endl;
	output->write(out.str());
}