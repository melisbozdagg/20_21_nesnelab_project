#include "Tank.h"
/**
*  constructor
*  @param cap capacity of tank
*/
Tank::Tank(double cap)
{
	this->capacity = cap;
}
/**
*  opens valve
*/
void Tank::open_valve()
{
	this->valve.openValve();
}
/**
*  closes valve
*/
void Tank::close_valve()
{
	this->valve.closeValve();
}
/**
*  breaks fuel tank
*/
void Tank::break_fuel_tank()
{
	this->broken = 1;
}
/**
*  repairss fuel tank
*/
void Tank::repair_fuel_tank()
{
	this->broken = 0;
}

double Tank::get_capacity()
{
	return capacity;
}

double Tank::get_fuel_quantity()
{
	return fuel_quantity;
}

bool Tank::get_broken()
{
	return broken;
}

bool Tank::get_valve_status()
{
	return this->valve.getStatus();
}
/**
*  prints information about current tank
*/
void Tank::print_info()
{
	Output* op = Output::getInstance();
	ostringstream out;
	out << endl << "Is broken: " << this->broken <<
		endl << "Capacity: " << this->capacity <<
		endl << "Fuel quanity: " << this->fuel_quantity <<
		endl << "Is valve open: " << this->valve.getStatus() << endl << endl;
	op->write(out.str());
}
/**
*  increases fuel quanity by given parameter
*  @param value value for increasing
*/
void Tank::increase_quanity(double value)
{
	this->fuel_quantity += value;
	if (this->fuel_quantity > this->capacity) {
		this->fuel_quantity = this->capacity;
	}
}
/**
*  observer update ant it notifys its valve too
*  @param index for printing index of tank
*/
void Tank::update(int index)
{
	ostringstream out;
	Output* output = Output::getInstance();
	out << "Tank " << index + 1 << ":Simulation stopped" << endl;
	output->write(out.str());
	this->valve.update(index);
}




