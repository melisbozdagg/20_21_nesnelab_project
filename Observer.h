#pragma once
/**
*  Observer abstract class
*/
class Observer {
public:
	virtual void update(int) = 0;
};