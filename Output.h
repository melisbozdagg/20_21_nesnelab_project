#pragma once
#include <iostream>
#include <fstream>
using namespace std;
/**
*  Output singleton class for access from everywhere of program
*/
class Output
{
private:
	fstream file;
	static Output* uniqueInstance;
	Output();
public:
	void openFile(string);
	void closeFile();
	static Output* getInstance();
	void write(string);
};

