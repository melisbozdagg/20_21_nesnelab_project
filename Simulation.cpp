#include "Simulation.h"
/**
* starts simulation by taking input and output file names and takes input lines. Calls run_lines function 
* @param in input file name
* @param out output file name
*/
void Simulation::start_simulation(string in, string out)
{
	Txt_File f1;
	if (f1.open_file(in))
		this->command_list = f1.input();

	f1.close_file();

	Output* output = Output::getInstance();
	output->openFile(out);

	this->run_lines();
}
/**
* stops simulation and notifys observers
*/
void Simulation::stop_simulation()
{
	Output* output = Output::getInstance();
	output->write("\nEnding simulation...\n");
	this->notify();
	exit(0);
}
/**
* prints a list of all connected tanks
*/
void Simulation::list_connected_tanks()
{
	ostringstream out;
	Output* output = Output::getInstance();
	for (int i = 0; i < this->all_tanks.size(); i++)
	{
		if (this->all_tanks[i] != NULL && this->all_tanks[i]->get_valve_status() == 1) {
			out << "Tank " << i + 1; 
			output->write(out.str());
			out.str("");
			this->all_tanks[i]->print_info();
		}
	}
}
/**
* adds fuel tank to simulation and adds as observer
* @param cap capacity of tank
*/
void Simulation::add_fuel_tank(double cap)
{
	Tank* tank = new Tank(cap);
	this->addObserver(tank);
	this->all_tanks.push_back(tank);
	this->number_of_tanks++;
}
/**
* prints number of tanks simulation have
*/
void Simulation::print_fuel_tank_count() {
	ostringstream out;
	Output* output = Output::getInstance();
	out << "Fuel tank count: " << this->number_of_tanks << endl;
	output->write(out.str());
}
/**
* prints all tanks simulation have
*/
void Simulation::list_fuel_tanks()
{
	ostringstream out;
	Output* output = Output::getInstance();
	for (int i = 0; i < this->all_tanks.size(); i++)
	{
		if (this->all_tanks[i] != NULL) {
			out << "Tank " << i + 1;
			output->write(out.str());
			out.str("");
			this->all_tanks[i]->print_info();
		}
	}
}
/**
* removes fuel tank from simulation and removes from observer list
* @param num index of tank
*/
void Simulation::remove_fuel_tank(int num)
{
	delete this->all_tanks[num];
	this->all_tanks[num] = NULL;
	this->removeObserver(num);
	this->number_of_tanks--;
}

/**
* understands and runs all the lines from input
*/
void Simulation::run_lines()
{
	ostringstream out;
	Output* output = Output::getInstance();
	string line;
	for (int i = 0; i < this->command_list.size(); i++)
	{
		line = this->command_list[i];
		if (line == "stop_simulation;") {
			this->stop_simulation();
		}
		if (line == "start_engine;") {
			this->engine->start_engine();
		}
		else if (line == "stop_engine;") {
			this->engine->stop_engine();
		}
		else if (line == "list_fuel_tanks;") {
			this->list_fuel_tanks();
			this->engine->burn_fuel(1);
		}
		else if (line == "print_fuel_tank_count;") {
			out << "Number of tanks: " << this->number_of_tanks << endl << endl;
			output->write(out.str());
			out.str("");
			this->engine->burn_fuel(1);
		}
		else if (line == "print_total_consumed_fuel_quantity;") {
			this->engine->print_total_consumed_fuel_quantity(time);
			this->engine->burn_fuel(1);
		}
		else if (line == "print_total_fuel_quantity;") {
			this->engine->print_total_fuel_quantity();
			this->engine->burn_fuel(1);
		}
		else {
			string buff, numbuff;
			for (int i = 0; line[i] != ';'; i++) {
				if (line[i] == ' ') {

					for (int j = i + 1; ; j++)
					{
						if (line[j] == ';')
							break;
						numbuff.push_back(line[j]);
					}

					if (buff == "give_back_fuel") {
						this->engine->burn_fuel(1);
						break;
					}
					else if (buff == "add_fuel_tank") {
						this->add_fuel_tank(stod(numbuff));
						break;
					}
					else if (buff == "open_valve") {
						this->all_tanks[stod(numbuff)-1]->open_valve();
						break;
					}
					else if (buff == "close_valve") {
						this->all_tanks[stod(numbuff)-1]->close_valve();
						break;
					}
					else if (buff == "print_tank_info") {
						out << "Tank " << stod(numbuff);
						output->write(out.str());
						out.str("");
						this->all_tanks[stod(numbuff)]->print_info();
						this->engine->burn_fuel(1);
						break;
					}
					else if (buff == "remove_fuel_tank") {
						this->remove_fuel_tank((int)stod(numbuff) - 1);
						this->engine->burn_fuel(1);
						break;
					}
					else if (buff == "connect_fuel_tank_to_engine") {
						this->engine->connect_tank(this->all_tanks[(int)stod(numbuff)-1]);
						this->all_tanks[(int)stod(numbuff)]->open_valve();
						break;
					}
					else if (buff == "disconnect_fuel_tank_from_engine") {
						this->engine->disconnect_tank(this->all_tanks[(int)stod(numbuff)-1]);
						this->all_tanks[(int)stod(numbuff) - 1]->close_valve();
						this->engine->burn_fuel(1);
						break;
					}
					else if (buff == "fill_tank") {
						string numbuff1, numbuff2;
						int k;
						for (k = 0; numbuff[k] != ' '; k++)
						{
							numbuff1.push_back(numbuff[k]);
						}
						for (k++; numbuff[k] != '\0'; k++)
						{
							numbuff2.push_back(numbuff[k]);
						}
						this->fill_tank((int)stod(numbuff1) - 1, stod(numbuff2));
						break;
					}
					else if (buff == "wait") {
						this->wait(stod(numbuff));
						break;
					}
				}
				buff.push_back(line[i]);
			}
		}
		this->time++;
	}
}
/**
* fills tank with given quanity
* @param tank_id id of the tank
* @param fuel_quanity quanity to fill
*/
void Simulation::fill_tank(int tank_id, double fuel_quantity)
{
	all_tanks[tank_id]->increase_quanity(fuel_quantity);
}
/**
* increases time
* @param sec increases time by this parameter
*/
void Simulation::wait(int sec) {
	this->time += sec;
	this->engine->burn_fuel(1);
}
/**
* adds observer to observer list
* @param obs takes observer pointer
*/
void Simulation::addObserver(Observer* obs)
{
	this->observers.push_back(obs);
}
/**
* removes an observer from list
* @param index index of the observer
*/
void Simulation::removeObserver(int index)
{
	this->observers[index] = NULL;
}
/**
* notifys all observers
*/
void Simulation::notify()
{
	this->engine->update();
	for (int i = 0; i < this->observers.size(); i++)
	{
		if (this->observers[i] != NULL)
			this->observers[i]->update(i);
	}
}
