#pragma once
#include "Observer.h"
#include <iostream>
#include "Output.h"
#include <sstream>
using namespace std;
/**
*  valve class for use in tank
*/
class Valve : public Observer
{
private:
	bool status = false;
public:
	bool getStatus();
	void openValve();
	void closeValve();
	void update(int);
};

