#include <iostream>
#include "Simulation.h"
using namespace std;

int main(int argc, char** argv) {

	Simulation simulation1;

	simulation1.start_simulation(argv[1], argv[2]);

	return 0;
}
