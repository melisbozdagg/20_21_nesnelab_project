#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include "Input.h"
using namespace std;
/**
*  Txt_File class opens and closes file
*/
class Txt_File {
private:
	fstream file;
public:
	Txt_File();
	~Txt_File();
	bool open_file(string file_name);
	vector<string> input();
	void close_file();
};