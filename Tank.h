#pragma once
#include <iostream>
#include <sstream>
#include "Valve.h"
#include "Observer.h"
#include "Output.h"
using namespace std;
/**
* Tank class
*/
class Tank : public Observer
{
public:
	Tank(double);
	void open_valve();
	void close_valve();
	void break_fuel_tank();
	void repair_fuel_tank();
	double get_capacity();
	double get_fuel_quantity();
	bool get_broken();
	bool get_valve_status();
	void print_info();
	void increase_quanity(double);
	void update(int);
private:
	double capacity;
	double fuel_quantity = 0;
	bool broken = 0;
	Valve valve;
};

