#pragma once
#include"Tank.h"
#include <vector>
#include <iostream>
#include <time.h>
#include <sstream>

using namespace std;
/**
*  singelton engine class
*/
class Engine
{
public:
	static Engine* getInstance();
	void start_engine();
	void stop_engine();
	void connect_tank(Tank*);
	void disconnect_tank(Tank*);
	void print_total_consumed_fuel_quantity(int time);
	void print_total_fuel_quantity();
	void give_back_fuel();
	double get_fuel_per_second();
	bool get_status();
	Tank* get_internal_tank();
	Tank* get_min_fuel_tank();
	Tank* absorbs_fuel();
	void burn_fuel(int);
	void update();
private :
	Engine();
	static Engine* uniqueInstance;
	const double fuel_per_second = 5.5;
	bool status = 0; // true means running
	vector<Tank*> tanks = {};
	Tank* internal_tank = new Tank(55.0);
};
