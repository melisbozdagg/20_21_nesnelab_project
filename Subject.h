#pragma once
#include "Observer.h"
/**
*  Subject abstract class
*/
class Subject {
public:
	virtual void addObserver(Observer*) = 0;
	virtual void removeObserver(int) = 0;
	virtual void notify() = 0;
};