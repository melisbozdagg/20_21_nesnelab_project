#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;
/**
*  Input class tankes input and passes to simulation
*/
class Input {
private:
	fstream* inp_file;
public:
	Input();
	~Input();
	void set_file(fstream& file);
	vector<string> read_input();
};
