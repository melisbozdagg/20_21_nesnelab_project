#include "Output.h"
Output* Output::uniqueInstance = 0;

Output::Output()
{
}

void Output::openFile(string str) {
	file.open(str);
}

void Output::closeFile() {
	this->file.close();
}
/**
*  for singleton
*/
Output* Output::getInstance()
{
	if (uniqueInstance == NULL) {
		uniqueInstance = new Output();
	}
	return uniqueInstance;
}
/**
*  wirtes to file
*/
void Output::write(string str)
{
	file << str;
}
