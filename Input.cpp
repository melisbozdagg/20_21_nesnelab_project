#include "Input.h"

Input::Input() {
	this->inp_file = NULL;
}
Input::~Input() {

}

void Input::set_file(fstream& file) {
	inp_file = &file;
};
vector<string> Input::read_input() {
	string line;
	vector<string> list;
	while (line != "stop_simulation;") {
		if (!getline(*inp_file, line)) {
			cout << "Error, end of file!!!\n";
			break;
		}
		list.push_back(line);
	}
	return list;
}
